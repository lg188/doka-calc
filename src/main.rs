use std::ops::{Add, Sub};

fn main() {
    let lander = Stats::new(47, 27, 2, 17, 130);

    // level 10
    let crawler = Stats::new(59, 36, 5, 8, 128);

    // level 1
    let rogue = Stats::new(7, 8, 1, 3, 21);
    
    // level 8
    let magician = Stats::new(24, 24, 20, 16, 100);

    // level 1
    let bandit  = Stats::new(4, 4, 4, 20, 18);


    let knife  = Stats::new(4, 0, 0, 0,0);

    println!("average {} vs {}", lander.average(), rogue.average());
}

#[derive(Debug)]
struct Stats {
    at: isize,
    df: isize,
    mg: isize,
    sp: isize,
    hp: isize,
}

impl Stats {
    pub fn new(at: isize, df: isize, mg: isize, sp: isize, hp: isize) -> Self {
        Self { at, df, mg, sp, hp }
    }

    pub fn average(&self) -> isize {
        (self.at + self.df + self.mg + self.sp + (self.hp / 10)) / 5
    }

    pub fn delta(&self) -> Stats {
        let avg = self.average();
        Stats::new(
            self.at - avg,
            self.df - avg,
            self.mg - avg,
            self.sp - avg,
            self.hp - (avg * 10),
        )
    }
}

impl Sub<Stats> for Stats {
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output {
        Self {
            at: self.at - other.at,
            df: self.df - other.df,
            mg: self.mg - other.mg,
            sp: self.sp - other.sp,
            hp: self.hp - other.hp,
        }
    }
}

impl Sub<isize> for Stats {
    type Output = Self;

    fn sub(self, other: isize) -> Self::Output {
        Self {
            at: self.at - other,
            df: self.df - other,
            mg: self.mg - other,
            sp: self.sp - other,
            hp: self.hp - other,
        }
    }
}

impl Add<Stats> for Stats {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        Self {
            at: self.at + other.at,
            df: self.df + other.df,
            mg: self.mg + other.mg,
            sp: self.sp + other.sp,
            hp: self.hp + other.hp,
        }
    }
}

impl Add<isize> for Stats {
    type Output = Self;

    fn add(self, other: isize) -> Self::Output {
        Self {
            at: self.at + other,
            df: self.df + other,
            mg: self.mg + other,
            sp: self.sp + other,
            hp: self.hp + other,
        }
    }
}
